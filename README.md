Prop | Description | Type | Default
------ | ------ | ------ | ------
**`activeDotIndex`** | Index of the currently active dot | Number | **Required**
**`dotsLength`** | Number of dots to display | Number | **Required**
`activeOpacity` | Opacity of the dot when tapped. The prop has no effect if `tappableDots` hasn't been set to `true`. | Number | 1